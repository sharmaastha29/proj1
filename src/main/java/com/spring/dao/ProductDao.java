package com.spring.dao;

import java.util.ArrayList;
import java.util.List;

//import org.springframework.beans.factory.annotation.Autowired;

import com.spring.model.Product;

public class ProductDao {
	
//	@Autowired
//	private Product product;
	
    List<Product> l=new ArrayList<Product>();
    {
    l.add(new Product(1, "Milk", 60, "amulgold"));
    l.add(new Product(2, "Ghee", 135, "Govardhan"));
    l.add(new Product(3, "Curd", 15, "milkymist"));
    l.add(new Product(4, "Aata", 120, "ashirwad"));
    }
    
    //Gets all the records of list
    public List<Product> list(){
    return l; 
    }
    
    //Gets a particular record based on id
    public Product get(int id){
    	for(Product product:l){
    	if(product.getId()==id)
    	return product;
    	}
    	return null;
    }
    
    //Creates a new entry of record in database/list for storage -- All mtds shld be public 
    public Product create (Product p){
    	l.add(p);
    	return p;
    }
    
    
    //delete an entry from db and return particular deleted id
    public int delete(int id){
    	for(Product pr:l){
    		if(pr.getId()==id){
    			l.remove(id);
    		}
    	}
    	return id;
    }
    
    //update an existing record in database and return updated record
    public Product update(int id, Product p){
    	for(Product pr:l){
    		if(pr.getId()==id){
    			p.setId(pr.getId());
    			l.remove(pr);
    			l.add(p);
    			return p;
    		}
    	}
    	
    	return null;
    }
    
    
    
    
     
	
	
}
